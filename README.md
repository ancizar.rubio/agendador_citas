Django api rest Autenticacion con Token rest-aut

installar dependencias para entorno virtual
"pip install requirements.txt"

bases de datos PostgreSql

http://127.0.0.1:8000/admin/               "ingreso de cuentas superadmin y ver el panel administrador de django"
                                           "python manage.py createsuperuser"

urls rest-auth

http://127.0.0.1:8000/auth/registration/   "Se puede obtener como en el login un Token de autenticacion"

http://127.0.0.1:8000/auth/login/          "Se obtiene un Token de autenticacion"

perfil de usuario

http://127.0.0.1:8000/auth/user/

se completa la tabla con datos como nombre y apellidos y puede editarlos

http://127.0.0.1:8000/auth/logout/         "Deslogueo de usuario y eliminacion del Token de autenticacion"

http://127.0.0.1:8000/agenda/lista/        "Lista de agendamientos que solo puede ver el administrador"

http://127.0.0.1:8000/agenda/post-agenda/  "Solo el usuario autenticado con Token puede generear una cita"

http://127.0.0.1:8000/agenda/updelete/2/   "Solo el usuario autenticado puede editar la informacion de su cita o eliminarla"

se establecen 3 modelos los cuales son Doctores, Tipo_Cita, Estado para aderirlos como foraneas al modelo de agenda, los cuales se debe ingresar los datos pertienentes para tener una opcion selectiva. Un ejemplo e Doctores se ingresa en el administrador el nombre de un Doctor al igual tipo de cita, Medico general, Odontolico y entre otros. El campo estado es manejado solo por el adminstrador y se ingreasaria informacion como Aprovado, Cancelado o Pendiente.

Para arrancar el back "python manage.py runserver 0.0.0.0:8000"






