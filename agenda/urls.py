from agenda.views import AgendaList, AgendaPost, AgendaUpD
from django.urls import path, include

urlpatterns = [
    path('lista/', AgendaList.as_view(), name="lista"),
    path('post-agenda/', AgendaPost.as_view(), name="post-agenda"),
    path('get-agenda/', AgendaPost.as_view(), name="get-agenda"),
    path('updelete/<id>/',AgendaUpD.as_view(), name="update-delete")
]