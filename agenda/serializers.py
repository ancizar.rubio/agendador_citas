from agenda.models import Agenda, Estado
from rest_framework import serializers



class EstadoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Estado
        fields = '__all__'

class AgendaSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Agenda
        read_only_fields = ('estado',)
        fields = ['usuario', 'fecha', 'doctor', 'cita']