from django.db import models
from django.contrib.auth.models import User
from doctores.models import Doctores, Tipo_Cita


# Create your models here.

class Estado(models.Model):
    estado_cita = models.CharField(verbose_name="Estado citas", max_length=50)
    
    class Meta:
        verbose_name = 'Estado'
        verbose_name_plural = 'Estados'
        
    def __str__(self):
        return self.estado_cita


class Agenda(models.Model):
    usuario = models.ForeignKey(User, verbose_name="Usuario", on_delete=models.CASCADE, null=False, blank=False)
    fecha =models.DateTimeField(verbose_name="Fecha para cita", auto_now=False, auto_now_add=False)
    doctor = models.ForeignKey(Doctores, verbose_name="Doctor", on_delete=models.CASCADE, null=False, blank=False)
    cita = models.ForeignKey(Tipo_Cita, verbose_name="Tipo de Cita", on_delete=models.CASCADE, null=False, blank=False)
    estado = models.ForeignKey(Estado, verbose_name="Estado de su cita", on_delete=models.CASCADE, null=True, blank=True)
    
    class Meta:
        verbose_name = 'Agenda'
        verbose_name_plural = 'Agendas'
        ordering = ['-id']