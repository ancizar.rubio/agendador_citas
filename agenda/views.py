from django.shortcuts import get_object_or_404
from agenda.serializers import AgendaSerializer
from rest_framework.response import Response
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.permissions import IsAdminUser
from agenda.models import Agenda
from rest_framework import status
from django.http import Http404

# Create your views here.

class AgendaList(APIView):
    permission_classes = [IsAdminUser]
    
    def get(self, request):
        queryset = Agenda.objects.all()
        serializer = AgendaSerializer(queryset, many=True)
        return Response(serializer.data)
    
class AgendaPost(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    
    def get(self, request):        
        queryset = Agenda.objects.all()
        serializer = AgendaSerializer(queryset, many=True)
    
        return Response(serializer.data)
    
    def post(self, reques):
        serializer = AgendaSerializer(data=reques.data, many=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    
class AgendaUpD(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    
    def get_object(self, id):
        try:
            return Agenda.objects.get(id=id)
        except Agenda.DoesNotExist:
            raise Http404

    def get(self, request, id, format=None):
        agenda = self.get_object(id)
        serializer = AgendaSerializer(agenda)
        return Response(serializer.data)

    def put(self, request, id, format=None):
        agenda = self.get_object(id)
        serializer = AgendaSerializer(agenda, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id, format=None):
        comprador = self.kwargs["id"]
        comprador = get_object_or_404(Agenda, id=id)
        comprador.delete()
        return Response(status=204)
        