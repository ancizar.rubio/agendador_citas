from agenda.models import Agenda, Estado
from django.contrib import admin

# Register your models here.

class AgendaAdmin(admin.ModelAdmin):
    model = Agenda
    list_display = ['usuario','fecha', 'doctor', 'cita', 'estado']
admin.site.register(Agenda,AgendaAdmin)

admin.site.register(Estado)