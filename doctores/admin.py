from doctores.models import Doctores, Tipo_Cita
from django.contrib import admin

# Register your models here.

admin.site.register(Doctores)
admin.site.register(Tipo_Cita)