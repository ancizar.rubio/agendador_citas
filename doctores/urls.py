from doctores.views import DoctresAPIView, Tipo_CitaAPIView
from django.urls import path

urlpatterns = [
    path('doctores/', DoctresAPIView.as_view(), name="doctores"),
    path('tipo-cita/', Tipo_CitaAPIView.as_view(), name="tipo-cita"),
]