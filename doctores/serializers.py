from doctores.models import Doctores, Tipo_Cita
from rest_framework import serializers


class DoctorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Doctores
        fields = '__all__'
        
        
class Tipo_CitaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tipo_Cita
        fields = '__all__'