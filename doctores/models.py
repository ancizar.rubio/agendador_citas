from django.db import models

# Create your models here.

class Doctores(models.Model):
    
    nombre = models.CharField(verbose_name="Nombre", max_length=50)
    
    class Meta:
        ordering = ['-id']
        
    def __str__(self):
        return self.nombre
        
class Tipo_Cita(models.Model):
    opcion = models.CharField(verbose_name="Opcion cita", max_length=50)

        
    def __str__(self):
        return self.opcion