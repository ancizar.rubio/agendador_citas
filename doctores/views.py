from doctores.serializers import DoctorSerializer, Tipo_CitaSerializer
from rest_framework.response import Response
from doctores.models import Doctores, Tipo_Cita
from django.shortcuts import render
from rest_framework.views import APIView

# Create your views here.

class DoctresAPIView(APIView):
    
    def get(self, request):        
        queryset = Doctores.objects.all()
        serializer = DoctorSerializer(queryset, many=True)
    
        return Response(serializer.data)
    
class Tipo_CitaAPIView(APIView):
    
    def get(self, request):        
        queryset = Tipo_Cita.objects.all()
        serializer = Tipo_CitaSerializer(queryset, many=True)
    
        return Response(serializer.data)